import axiosClient from "./axiosClient";

export const getALlList = (payload) => {
    return axiosClient({
        url: "/getallticket",
        method: "POST",
        data: payload
    })
}

export const insertTicketsToSql = (payload) => {
    return axiosClient({
        url: "/insertdb",
        method: 'POST',
        data: payload
    })
}

export const getALlGroup = (payload) => {
    return axiosClient({
        url: "/getallgroup",
        method: 'POST',
        data: payload
    })
}

export const insertGroupsToSql = (payload) => {
    return axiosClient({
        url: "/insertGroup",
        method: 'POST',
        data: payload
    })
}