import axios from 'axios'
const axiosClient = axios.create({
    // baseURL: "https://request.base.vn/extapi/v1/request",
    baseURL: "http://localhost:4000",
    headers: {
        'Content-Type': 'application/json',
    },
})

export default axiosClient
