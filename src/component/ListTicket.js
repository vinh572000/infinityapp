import { useEffect, useRef, useState } from "react";
import { getALlList, insertTicketsToSql } from "../api/listApi";
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCardTitle, CCol, CFormInput, CFormSelect, CRow, CWidgetStatsA } from '@coreui/react';
import { CSmartPagination, CSmartTable } from '@coreui/react-pro';
import { useNavigate } from "react-router-dom";
import { convertDateForm } from "../helper/helper";
import { Button } from "@coreui/coreui";

function ListTicket() {

  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState([]);
  const [page, setPage] = useState(0);
  const [countPage, setCountPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(20);

  const [listTicket, setListTicket] = useState([]);
  const [newPage, setNewPage] = useState(0)

  let tempData = [];


  const bottomEl = useRef(null);

  const scrollToBottom = () => {
    bottomEl?.current?.scrollIntoView({ behavior: 'smooth' });
  };
  // const getAllList = async (pageAgr = page) => {
  const getAllList = async (pageAgr = newPage) => {
    setLoading(true);
    let params = {
      page: pageAgr,
      status: "pending",
      access_token: "8369-DKNY3YZS4MX6Z7UPQT96S2C34RUD7HJA6FKBN5B5ERQJ5F2NH78XCSMVJMMBM2SS-VHV7TJDWZX3JG3JMFTW5LH4RPR6XEAPBRER8AW2UN62PSFWJJD6NTPDA8SU3GTA8"
    }

    let res = await getALlList(params);
    if (res.data.todoData.requests) {
      setItems(res.data.todoData.requests);
      setListTicket([]);
      res.data.todoData.requests.forEach((ticket, index) => {
        let tempticket = {};
        tempticket.id = ticket.id
        tempticket.name = ticket.name;
        tempticket.content = ticket.content;
        tempticket.file_names = ticket.file_names;
        tempticket.rejecters = ticket.rejecters || [];
        tempticket.approvals = ticket.approvals || [];
        // if (ticket.approvals.length) {
        //   tempticket.approval_final = ticket.approvals[ticket.approvals.length - 1];
        // } else {
        //   tempticket.approval_final = "";
        // }
        tempticket.username = ticket.username
        tempticket.full_name_created = ticket.results[0].note.split('</b>')[0].replace("<b>", "");
        tempticket.since = ticket.since;
        tempticket.last_update = ticket.last_update;
        let temp = [];
        let tempOwners = [];
        ticket.owners.forEach((item, index) => {
          tempOwners.push(item.username);
          if (item.approved === 0) {
            temp.push(item.username);
          }
        })
        if (temp.length > 0) {
          tempticket.approval_next = temp[0];
        } else {
          tempticket.approval_next = ticket.owners[0].username;
        }
        tempticket.owners = tempOwners;

        if (ticket.approvals.length) {
          tempticket.approval_final = tempOwners[tempOwners.length - 1];
        } else {
          tempticket.approval_final = "";
        }

        let flag = tempData.some((item) => {
          return item.id === tempticket.id
        })
        if (!flag) {
          tempData.push(tempticket);
        }
      })
      setListTicket(tempData);
      setLoading(false);
      return res.data.todoData.requests.length;
    } else {
      return [];
    }
  }

  useEffect(() => {
    getAllList();
  }, [page])



  const InsertToSql = () => {
    //console.log(listTicket);
    // return;
    insertTicketsToSql({ tickets: listTicket });
    scrollToBottom();
  }

  const nextPageOK = () => {
    setNewPage(newPage + 1);
    getAllList(newPage + 1);
  }

  const columns = [
    // { key: "id", label: "ID", _style: { textAlign: "left" }, filter: true },
    { key: "name", label: "Name", _style: { textAlign: "left", width: '10%' }, filter: true },
    { key: "content", label: "Content", _style: { textAlign: "left", width: '50%' }, filter: true },
    { key: "file_names", label: "File Names", _style: { textAlign: "left", width: '20%' }, filter: true },
    { key: "rejecters", label: "Rejecters", _style: { textAlign: "left" }, filter: true },
    { key: "approvals", label: "Approvals", _style: { textAlign: "left" }, filter: true },
    { key: "username", label: "User", _style: { textAlign: "left", }, filter: true },
    { key: "since", label: "Since", _style: { textAlign: "left", }, filter: true },
    { key: "last_update", label: "Last Update", _style: { textAlign: "left", }, filter: true },
  ];

  return (
    <div>
      <CCard>
        <CCardHeader>
          <CCardTitle>
            <CButton onClick={InsertToSql}>
              Insert to DB
            </CButton>
          </CCardTitle>
        </CCardHeader>
        <CCardBody>
          {
            !loading ? (
              <div>
                <CSmartTable
                  activePage={1}
                  items={items}
                  columns={columns}
                  itemsPerPage={itemsPerPage}
                  page={page}
                  noItemsLabel={loading && items.length === 0 ? 'Đang load dữ liệu' : !loading && items.length === 0 ? 'Không có dữ liệu' : '.....'}
                  columnFilter
                  scopedColumns={{
                    content: (item) => <td>{item.content.split('<br>').map((para, index) => {
                      return <p style={{ textAlign: 'left' }} key={index}>{para}</p>

                    })}</td>,
                    file_names: (item) => <td>{item.file_names.split('<br>').map((file_name, index) => {
                      return <p style={{ textAlign: 'left' }} key={index}>{file_name}</p>

                    })}</td>,
                    approvals: (item) => <td>{item.approvals.join(',')}</td>,
                    rejecters: (item) => <td>{item.approvals.join(',')}</td>,
                    since: (item) => <td>{convertDateForm(new Date(item.since * 1000))}</td>,
                    last_update: (item) => <td>{convertDateForm(new Date(item.last_update * 1000))}</td>
                  }}
                  clickableRows={true}
                  onItemsPerPageChange={setItemsPerPage}
                  // onRowClick={(e) => {
                  //   navigate(`/detail_ticket/${e.id}`);
                  // }}
                  tableProps={{
                    striped: true,
                    hover: true,
                    responsive: true,
                  }}
                />
                {/* <CSmartPagination
                  pages={countPage + 1}
                  activePage={page + 1}
                  onActivePageChange={async (i) => {
                    const dataTableLength = await getAllList(i - 1);
                    setPage(i - 1);
                    if (i >= countPage && dataTableLength === itemsPerPage) {
                      setCountPage(i);
                    }
                  }}

                /> */}
              </div>

            ) : (
              <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            )
          }

        </CCardBody>
        <CCardFooter>
          <CButton onClick={nextPageOK}>
            Next Page
          </CButton>
          <p>Đang ở page {newPage + 1}</p>
          <div ref={bottomEl}></div>
        </CCardFooter>
      </CCard>

    </div>
  )
}

export default ListTicket;