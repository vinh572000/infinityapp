import React, { useEffect, useRef, useState } from 'react'
import { getALlGroup, insertGroupsToSql } from '../api/listApi';
import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCardTitle } from '@coreui/react';
import { CSmartTable } from '@coreui/react-pro';

function ListGroup() {

    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(0);
    const [items, setItems] = useState([]);
    const [newPage, setNewPage] = useState(0)
    const [listGroup, setListGroup] = useState([]);
    let tempData = [];

    const bottomEl = useRef(null);

    const scrollToBottom = () => {
        bottomEl?.current?.scrollIntoView({ behavior: 'smooth' });
    };

    const getGroup = async (pageAgr = newPage) => {
        setLoading(true);
        let params = {
            page: pageAgr,
            access_token: "8369-DKNY3YZS4MX6Z7UPQT96S2C34RUD7HJA6FKBN5B5ERQJ5F2NH78XCSMVJMMBM2SS-VHV7TJDWZX3JG3JMFTW5LH4RPR6XEAPBRER8AW2UN62PSFWJJD6NTPDA8SU3GTA8"
        }

        let res = await getALlGroup(params);
        if (res.data.todoData.groups.length > 0) {
            //setItems(res.data.todoData.groups);
            res.data.todoData.groups.forEach((group, index) => {
                let tempGroup = {};
                tempGroup.id = group.id;
                tempGroup.gid = group.gid;
                tempGroup.username = group.username;
                tempGroup.webhooks = group.webhooks;
                // console.log('webhooks', group.webhooks);
                tempGroup.category = group.category;
                tempGroup.content = group.content;
                tempGroup.name = group.name;

                tempGroup.permission_edit_followers = group.options.permission_edit_followers;
                tempGroup.fix_followers = group.options.fix_followers;
                tempGroup.add_task_follower = group.options.add_task_follower;
                tempGroup.approved_block_edit_comment = group.options.approved_block_edit_comment;
                tempGroup.allow_followers_export_action = group.options.allow_followers_export_action;
                tempGroup.allow_approvers_export_action = group.options.allow_approvers_export_action;
                //tempGroup.allow_return_request = group.allow_return_request;
                if (group.allow_return_request === 1) {
                    tempGroup.allow_return_request = "yes"
                } else {
                    tempGroup.allow_return_request = "no"
                }

                if (group.allow_transfer_approver === 1) {
                    tempGroup.allow_transfer_approver = "yes"
                } else {
                    tempGroup.allow_transfer_approver = "no"
                }

                //tempGroup.allow_transfer_approver = group.allow_transfer_approver;
                tempGroup.notis_sequential = group.options.notis_sequential;


                if (group.email === 1) {
                    tempGroup.email = "yes"
                } else {
                    tempGroup.email = "no"
                }

                let flag = tempData.some((item) => {
                    return item.id === tempGroup.id
                })
                if (!flag) {
                    tempData.push(tempGroup);
                }
                setListGroup(tempData);
                setItems(tempData);

                setLoading(false);

            })
        } else {
            setItems([]);
            setLoading(false);
        }

    }

    useEffect(() => {
        getGroup();
    }, []);



    const InsertToSql = () => {
        insertGroupsToSql({ groups: listGroup });
        scrollToBottom();
    }

    const nextPageOK = () => {
        setNewPage(newPage + 1);
        getGroup(newPage + 1);
    }

    const columns = [
        { key: "id", label: "id", _style: { textAlign: "left" }, filter: true },
        { key: "gid", label: "gid", _style: { textAlign: "left" }, filter: true },
        { key: "username", label: "username", _style: { textAlign: "left" }, filter: true },
        //{ key: "webhooks", label: "webhooks", _style: { textAlign: "left" }, filter: true },
        { key: "category", label: "category", _style: { textAlign: "left" }, filter: true },
        { key: "content", label: "content", _style: { textAlign: "left", }, filter: true },
        { key: "name", label: "name", _style: { textAlign: "left", }, filter: true },
        { key: "permission_edit_followers", label: "permission_edit_followers", _style: { textAlign: "left", }, filter: true },
        { key: "fix_followers", label: "fix_followers", _style: { textAlign: "left", }, filter: true },
        { key: "add_task_follower", label: "add_task_follower", _style: { textAlign: "left", }, filter: true },
        { key: "approved_block_edit_comment", label: "approved_block_edit_comment", _style: { textAlign: "left", }, filter: true },
        { key: "allow_approvers_export_action", label: "allow_approvers_export_action", _style: { textAlign: "left", }, filter: true },
        { key: "allow_followers_export_action", label: "allow_followers_export_action", _style: { textAlign: "left", }, filter: true },
        { key: "allow_return_request", label: "allow_return_request", _style: { textAlign: "left", }, filter: true },
        { key: "allow_transfer_approver", label: "allow_transfer_approver", _style: { textAlign: "left", }, filter: true },
        { key: "notis_sequential", label: "notis_sequential", _style: { textAlign: "left", }, filter: true },
        { key: "email", label: "email", _style: { textAlign: "left", }, filter: true },
    ];

    return (
        <div>
            <CCard>
                <CCardHeader>
                    <CCardTitle>
                        <CButton onClick={InsertToSql}>
                            Insert to DB
                        </CButton>
                    </CCardTitle>
                </CCardHeader>
                <CCardBody>
                    {
                        !loading ? (
                            <div>
                                <CSmartTable
                                    activePage={1}
                                    items={items}
                                    columns={columns}
                                    itemsPerPage={20}
                                    page={page}
                                    noItemsLabel={loading && items.length === 0 ? 'Đang load dữ liệu' : !loading && items.length === 0 ? 'Không có dữ liệu' : '.....'}
                                    columnFilter
                                    // scopedColumns={{
                                    //     content: (item) => <td>{item.content.split('<br>').map((para, index) => {
                                    //         return <p style={{ textAlign: 'left' }} key={index}>{para}</p>

                                    //     })}</td>,
                                    //     file_names: (item) => <td>{item.file_names.split('<br>').map((file_name, index) => {
                                    //         return <p style={{ textAlign: 'left' }} key={index}>{file_name}</p>

                                    //     })}</td>,
                                    //     approvals: (item) => <td>{item.approvals.join(',')}</td>,
                                    //     rejecters: (item) => <td>{item.approvals.join(',')}</td>,
                                    //     since: (item) => <td>{convertDateForm(new Date(item.since * 1000))}</td>,
                                    //     last_update: (item) => <td>{convertDateForm(new Date(item.last_update * 1000))}</td>
                                    // }}
                                    clickableRows={true}
                                    // onItemsPerPageChange={setItemsPerPage}
                                    // onRowClick={(e) => {
                                    //   navigate(`/detail_ticket/${e.id}`);
                                    // }}
                                    tableProps={{
                                        striped: true,
                                        hover: true,
                                        responsive: true,
                                    }}
                                />
                                {/* <CSmartPagination
                  pages={countPage + 1}
                  activePage={page + 1}
                  onActivePageChange={async (i) => {
                    const dataTableLength = await getAllList(i - 1);
                    setPage(i - 1);
                    if (i >= countPage && dataTableLength === itemsPerPage) {
                      setCountPage(i);
                    }
                  }}

                /> */}
                            </div>

                        ) : (
                            <div className="spinner-border" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        )
                    }

                </CCardBody>
                <CCardFooter>
                    <CButton onClick={nextPageOK}>
                        Next Page
                    </CButton>
                    <p>Đang ở page {newPage + 1}</p>
                    <div ref={bottomEl}></div>
                </CCardFooter>
            </CCard>
        </div>
    )
}

export default ListGroup
