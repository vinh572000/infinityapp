import './App.css';
import '@coreui/coreui/dist/css/coreui.min.css'
import ListTicket from './component/ListTicket';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import DetailTicket from './component/DetailTicket';
import React from 'react';
import ListGroup from './component/ListGroup';
function App() {
  return (
    <React.StrictMode>
      <BrowserRouter>
        <div className="App">
          <Routes>
            <Route exact path='/' element={<ListTicket />} />
            <Route exact path='group_list' element={<ListGroup />} />
            <Route path='/detail_ticket/:id' element={<DetailTicket />} />
            <Route path="*" element={<p>Path not resolved</p>} />
          </Routes>
        </div>
      </BrowserRouter>
    </React.StrictMode>
  );
}

export default App;
