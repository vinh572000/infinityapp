export const convertDateForm = (time) => {
    if (time) {
        if (typeof time === "object") {
            return (
                time.getDate().toString().padStart(2, 0) +
                "/" +
                (time.getMonth() + 1).toString().padStart(2, "0") +
                "/" +
                time.getFullYear()
            );
        } else if (typeof time === "string") {
            // time = time.split("/").reverse().join("/");
            return new Date(time);
        }
    }

    return null;
};
